package es.cursos.mainPrueba;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import es.cursos.clases.Alumno;
import es.cursos.clases.Curso;
import es.cursos.clases.CursoOnline;
import es.cursos.clases.CursoPresencial;


public class Main {
	static Map <String,Alumno> listaAlumnos=new HashMap<String,Alumno>();
	static Map <String,Curso> listaCursos=new HashMap<String,Curso>();
	
	public static void main(String args[]) throws ParseException {
		SimpleDateFormat formato=new SimpleDateFormat("dd-MM-yyyy");
		//Primer paso
		listaAlumnos.put("34678904",new Alumno("Pepe","34678904"));
		System.out.println("");
		//Segundo paso
		listaAlumnos.put("17679456", new Alumno("Andrea","17679456",125));
		System.out.println("");
		//Tercer paso
		listaCursos.put("Dise�o de Bases de Datos", new CursoPresencial("Dise�o de Bases de Datos",formato.parse("5-05-2014"),formato.parse("5-05-2014"),1,50,20,1));
		System.out.println("");
		//Cuarto paso
		try {
			CursoOnline cursoActual=new CursoOnline("Administraci�n de Bases de Datos", formato.parse("12-05-2014"), formato.parse("16-05-2014"), 5, 25, 4);
			cursoActual.getCursosPrevios().add(buscarCurso("Dise�o de Bases de Datos"));
			listaCursos.put("Administraci�n de Bases de Datos", cursoActual);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("");
		//Quinto paso
		if(buscarCurso("Dise�o de Bases de Datos").matricularAlumno(buscarAlumno("34678904"))) {
			System.out.println("Alumno matriculado");
		}
		if(buscarCurso("Dise�o de Bases de Datos").matricularAlumno(buscarAlumno("17679456"))) {
			System.out.println("Alumno matriculado");
		}
		System.out.println("");
		//Sexto paso
			if(((CursoPresencial) buscarCurso("Dise�o de Bases de Datos")).registroAsistencia(1,buscarAlumno("34678904"))) {
				System.out.println("Asistencia registrada");
			}
			else {
				System.out.println("Fallo de registro de asistencia");
			}
			System.out.println("");
		//S�ptimo paso
			if(buscarCurso("Dise�o de Bases de Datos").calificar(null)) {
				System.out.println("Evaluaci�n terminada");
			}
			System.out.println("Alumnos aptos");
			for(Alumno aAux:buscarCurso("Dise�o de Bases de Datos").getAlumnosAptos()) {
				System.out.println(aAux.getNombre());
			}
			System.out.println("----------------------------------");
			System.out.println("");
		//Octavo paso
			if(buscarCurso("Administraci�n de Bases de Datos").matricularAlumno(buscarAlumno("34678904"))) {
				System.out.println("El alumno "+buscarAlumno("34678904").getNombre()+" ha sido matriculado");
			}
			else {
				System.out.println("El alumno "+buscarAlumno("34678904").getNombre()+" no ha sido matriculado");
			}
			if(buscarCurso("Administraci�n de Bases de Datos").matricularAlumno(buscarAlumno("17679456"))) {
				System.out.println("El alumno "+buscarAlumno("17679456").getNombre()+" ha sido matriculado");
			}
			else {
				System.out.println("El alumno "+buscarAlumno("17679456").getNombre()+" no ha sido matriculado");
			}
			System.out.println("");
		//Noveno paso
			ArrayList <Alumno> alumnosMatriculados=buscarCurso("Administraci�n de Bases de Datos").getAlumnosMatriculados();
			System.out.println("Alumnos matriculados en Administraci�n de Bases de Datos");
			for(Alumno alumnoActual:alumnosMatriculados) {
				System.out.println(alumnoActual.toString());
			}
			System.out.println("-------------------------------------------------");
			System.out.println("");
			
		//D�cimo paso
			if(((CursoOnline) buscarCurso("Administraci�n de Bases de Datos")).superarNivel(buscarAlumno("34678904"))) {
				System.out.println("El alumno "+buscarAlumno("34678904").getNombre()+" ha superado el nivel");
			}
			System.out.println("");
		//Und�cimo paso
			System.out.println("Lista de los alumnos aptos de Administraci�n de Bases de Datos");
			ArrayList <Alumno> alumnosAptos=buscarCurso("Administraci�n de Bases de Datos").getAlumnosAptos();		
			for(Alumno alumnoActual:alumnosAptos) {
				System.out.println(alumnoActual.toString());
			}
			System.out.println("-----------------------------------------------------------------");
			System.out.println("");
		//Duod�cimo paso
			System.out.println("Lista de alumnos ordenados por DNI");
			ArrayList<Alumno> sadas = buscarCurso("Dise�o de Bases de Datos").getAlumnosMatriculados();
			sadas.sort(Comparator.comparing(Alumno::getDni));
			for (Alumno alumno : sadas) {
				System.out.println(alumno.getDni()+" - "+alumno.getNombre());
			}
			
				
	}
	
	static public Alumno buscarAlumno(String dni) {
		for(Map.Entry<String, Alumno> alumno:listaAlumnos.entrySet()){
			if(alumno.getKey().equals(dni)) {
				return alumno.getValue();
			}
		}
		return null;
	}

	static public Curso buscarCurso(String nombre) {
		for(Map.Entry<String, Curso> cursoActual:listaCursos.entrySet()){
			if(cursoActual.getKey().equals(nombre)) {
				return cursoActual.getValue();
			}
		}
		return null;
	}
}
