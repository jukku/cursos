package es.cursos.clases;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CursoPresencial extends Curso {
	private int cupo;
	private int N_MIN_ASISTENCIAS;
	private HashMap<Integer, HashMap<String, Alumno>> asistencias=new HashMap<Integer,HashMap<String,Alumno>>();
	@SuppressWarnings("unused")
	private int plazasLibres;

	public CursoPresencial(String titulo, Date fechaInicio, Date fechaFinalizacion, int nDias, double precioMatricula,int cupo, int N_MIN_ASISTENCIAS) {
		super(titulo, fechaInicio, fechaFinalizacion, N_MIN_ASISTENCIAS, precioMatricula);
		this.cupo = cupo;
		this.N_MIN_ASISTENCIAS = N_MIN_ASISTENCIAS;
		this.plazasLibres=cupo;
	}

	public boolean registroAsistencia(int dia, Alumno alumno) {
		if (dia <= getnDias() && alumnoEstaMatriculado(alumno)) {// El dia es correcto y el alumno esta matriculado
/*			int falta = (int) (Math.random() * 10 + 1);//Faltas aleatorias
			if (falta > 1) { // El alumno no ha faltado
			}
		}*/
			if(!asistencias.containsKey(dia)){
				asistencias.put(dia, new HashMap<String,Alumno>());
			}
			HashMap<String,Alumno> mapAux=asistencias.get(dia);
			mapAux.put(alumno.getDni(), alumno);
			asistencias.replace(dia, mapAux);
			return true;
		}
		return false;
	}

	public int consultaAsistencias (Alumno alumno) {
		int contador=0;
		for(Map.Entry<Integer,HashMap<String, Alumno>> diaActual:asistencias.entrySet()) {
			for(Map.Entry<String,Alumno> alu:diaActual.getValue().entrySet()) {
				if(alu.getValue().equals(alumno)) {
					contador++;
				}
			}
			
		}
		return contador;
	}

	public boolean alumnoEstaMatriculado(Alumno alumno) {
		for (Alumno alum : getAlumnosMatriculados()) {
			if (alum.getDni().equals(alumno.getDni())) {
				return true;
			}
		}
		return false;

	}

	@Override
	public boolean matricularAlumno(Alumno alumno) {
		if(this.cupo-getAlumnosMatriculados().size() > 0) {
			if(alumno.getCredito()>=getPrecioMatricula()) {
				alumno.decrementarCredito(getPrecioMatricula());
				alumno.agregarCurso(this);
				this.getAlumnosMatriculados().add(alumno);
				this.setnAlumnosMatriculados(this.getnAlumnosMatriculados()+1);
				this.plazasLibres--;
				return true;
			}
			else {
				System.out.println("Mu pobre");
			}
		}
		else {
			System.out.println("Curso lleno");
		}
		return false;
	}

	@Override
	public boolean calificar(Alumno alumno) { //Creo que no debe recibir nada el m�todo
		for (Alumno alu: getAlumnosMatriculados()) {
			System.out.println("El alumno "+alu.getNombre()+" ha asistido "+consultaAsistencias(alu)+" veces.");
			if(consultaAsistencias(alu)>= N_MIN_ASISTENCIAS) {//El alumno es apto
				getAlumnosAptos().add(alu);
			}
			else {
				System.out.println("El alumno "+alu.getNombre()+" no es apto");
			}
		}
		return true;
	}

	@Override
	public Curso clone() {
		CursoPresencial clon = new CursoPresencial(this.getTitulo(),this.getFechaInicio(),this.getFechaFinalizacion(),this.getnDias(),this.getPrecioMatricula(),this.cupo,this.N_MIN_ASISTENCIAS);
		return clon;
	}

}
