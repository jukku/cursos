package es.cursos.clases;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class CursoOnline extends Curso {
	private final int NIVEL;
	private HashMap<String,Integer> seguimientoAlumnos;
	private ArrayList<Curso> cursosPrevios;

	public CursoOnline(String titulo, Date fechaInicio, Date fechaFinalizacion, int nDias, double precioMatricula, int nIVEL) {
		super(titulo, fechaInicio, fechaFinalizacion, nDias, precioMatricula);
		NIVEL = nIVEL;
		this.seguimientoAlumnos = new HashMap<String,Integer>();
		cursosPrevios = new ArrayList<Curso>();
	}
	//Para clone()
	public CursoOnline(String titulo, Date fechaInicio, Date fechaFinalizacion, int nDias, double precioMatricula, int nIVEL,ArrayList<Curso>cursosPrevios) {
		super(titulo, fechaInicio, fechaFinalizacion, nDias, precioMatricula);
		NIVEL = nIVEL;
		this.seguimientoAlumnos = new HashMap<String,Integer>();
		this.cursosPrevios = cursosPrevios;
	}

	public ArrayList<Curso> getCursosPrevios(){
		return this.cursosPrevios;
	}
	
	@Override
	public boolean matricularAlumno(Alumno alumno) {
		for(Curso curso:cursosPrevios) {
			if(!curso.alumnoEsApto(alumno)||alumno.getCursosMatriculados().indexOf(curso)==-1) {
				return false;
			}
		}
		this.seguimientoAlumnos.put(alumno.getDni(),0);
		this.setnAlumnosMatriculados(this.getnAlumnosMatriculados()+1);
		this.getAlumnosMatriculados().add(alumno);
		alumno.getCursosMatriculados().add(this);
		alumno.decrementarCredito(this.getPrecioMatricula());
		return true;
	}

	@Override
	public boolean calificar(Alumno alumno) {
		if(!this.getAlumnosMatriculados().contains(alumno) || NIVEL/2>this.seguimientoAlumnos.get(alumno.getDni())) {
			return false;
		}		
		this.getAlumnosAptos().add(alumno);
		return true;
	}

	public boolean superarNivel(Alumno alu) {
		if((seguimientoAlumnos.containsKey(alu.getDni())) && seguimientoAlumnos.get(alu.getDni())<NIVEL) {
			seguimientoAlumnos.replace(alu.getDni(), seguimientoAlumnos.get(alu.getDni())+1);
			return true;
		}
		return false;
	}
	
	public int consultarNivel(Alumno alumno) {
		if(this.seguimientoAlumnos.containsKey(alumno.getDni())) {
			return this.seguimientoAlumnos.get(alumno.getDni());
		}
		else {
			return -1;			
		}
	}

	@Override
	public Curso clone() {
		CursoOnline clon = new CursoOnline(this.getTitulo(),this.getFechaInicio(),this.getFechaFinalizacion(),this.getnDias(),this.getPrecioMatricula(),this.NIVEL,this.cursosPrevios);
		return clon;
	}
	
}
