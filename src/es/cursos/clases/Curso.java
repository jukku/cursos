package es.cursos.clases;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public abstract class Curso implements Cloneable{
	private String titulo;
	private Date fechaInicio;
	private Date fechaFinalizacion;
	private int nDias;
	private double precioMatricula;
	private ArrayList<Alumno> alumnosMatriculados;
	private ArrayList<Alumno> alumnosAptos;
	private int nAlumnosMatriculados;
	
	public Curso(String titulo, Date fechaInicio, Date fechaFinalizacion, int nDias, double precioMatricula) {
		super();
		this.titulo = titulo;
		this.fechaInicio = fechaInicio;
		this.fechaFinalizacion = fechaFinalizacion;
		this.nDias = nDias;
		this.precioMatricula = precioMatricula;
		this.alumnosMatriculados = new ArrayList<Alumno>();
		this.alumnosAptos = new ArrayList<Alumno>();
		this.nAlumnosMatriculados = 0;
	}

	public String getTitulo() {
		return titulo;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}

	public int getnDias() {
		return nDias;
	}

	public double getPrecioMatricula() {
		return precioMatricula;
	}

	public ArrayList<Alumno> getAlumnosMatriculados() {
		return alumnosMatriculados;
	}

	public ArrayList<Alumno> getAlumnosAptos() {
		return alumnosAptos;
	}

	public int getnAlumnosMatriculados() {
		return nAlumnosMatriculados;
	}
	
	public void setnAlumnosMatriculados(int nAlumnosMatriculados) {
		this.nAlumnosMatriculados=nAlumnosMatriculados;
	}
	
	public boolean cursoTerminado() {
		Calendar fechaActualCalendar=Calendar.getInstance();
		Date fechaActual=fechaActualCalendar.getTime();
		int resultado=fechaActual.compareTo(this.fechaFinalizacion);
		if(resultado>0) {
			return true;
		}
		else {
			return false;			
		}		
	}
	public boolean alumnoEsApto(Alumno alu) {
		for (Alumno alumno : alumnosAptos) {
			if(alu.getDni().equals(alumno.getDni())) {
				return true;
			}
		}
		return false;
	}
	
	public abstract boolean matricularAlumno(Alumno alumno);
	
	public abstract boolean calificar(Alumno alumno);
	
	@Override
	public abstract Curso clone();
	
}
