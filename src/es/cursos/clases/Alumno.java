package es.cursos.clases;

import java.util.ArrayList;
import java.util.Comparator;

public class Alumno  implements Comparator<Alumno>{
	private String nombre;
	private String dni;
	private double credito=0;
	private ArrayList<Curso> cursosMatriculados=null;
	
	
	
	public Alumno(String nombre, String dni) {
		super();
		this.nombre = nombre;
		this.dni = dni;
		this.cursosMatriculados = new ArrayList<Curso>();
		this.credito=100;
	}

	public Alumno(String nombre, String dni, double credito) {
		super();
		this.nombre = nombre;
		this.dni = dni;
		this.credito = credito;
		this.cursosMatriculados = new ArrayList<Curso>();
	}

	public String getNombre() {
		return nombre;
	}

	public String getDni() {
		return dni;
	}

	public double getCredito() {
		return credito;
	}
	
	public ArrayList<Curso> getCursosMatriculados() {
		return cursosMatriculados;
	}

	public void incrementarCredito(double cantidad) {
		this.credito += cantidad;
	}
	
	public void decrementarCredito(double cantidad) {
		this.credito -= cantidad;
	}
	
	public void agregarCurso(Curso curso) {
		cursosMatriculados.add(curso);
	}

	@Override
	public String toString() {
		return "nombre=" + nombre + ", dni=" + dni + ", credito=" + credito;
	}

	public int compare(Alumno arg0, Alumno arg1) {
		return arg0.getDni().compareTo(arg1.getDni());
		
	}
	
	
}
